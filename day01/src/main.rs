use std::{error::Error, fs};

fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    let input = fs::read_to_string("input")?;
    let fuel_total: i32 = input
        .split('\n')
        .filter_map(|s| s.parse::<i32>().ok())
        .map(get_fuel)
        .sum();
    println!("{}", fuel_total);
    Ok(())
}

fn get_fuel(mass: i32) -> i32 {
    let fuel = mass / 3 - 2;
    if fuel <= 0 {
        0
    } else {
        fuel + get_fuel(fuel)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_get_fuel() {
        assert_eq!(get_fuel(12), 2);
        assert_eq!(get_fuel(14), 2);
        assert_eq!(get_fuel(1969), 966);
        assert_eq!(get_fuel(100756), 50346);
    }
}
