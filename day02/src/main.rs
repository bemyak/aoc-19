use std::{error::Error, fs};

fn main() -> Result<(), Box<dyn Error>> {
    let input = fs::read_to_string("input")?;
    let program = input
        .split(',')
        .filter_map(|s| s.parse::<usize>().ok())
        .collect::<Vec<_>>();

    for noun in 0..=99 {
        for verb in 0..=99 {
            let mut program = program.clone();
            program[1] = noun;
            program[2] = verb;
            execute(&mut program);
            if program[0] == 19690720 {
                println!("{}", 100 * noun + verb);
                return Ok(());
            }
        }
    }
    Ok(())
}

fn execute(program: &mut Vec<usize>) {
    for addr in (0..program.len()).step_by(4) {
        let instr = program[addr];
        match instr {
            1 => {
                let x = program[program[addr + 1]];
                let y = program[program[addr + 2]];
                let res_addr = program[addr + 3];
                program[res_addr] = x + y;
            }
            2 => {
                let x = program[program[addr + 1]];
                let y = program[program[addr + 2]];
                let res_addr = program[addr + 3];
                program[res_addr] = x * y;
            }
            99 => {}
            _ => panic!("Unknown command: {}", instr),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_execute() {
        let mut t1 = vec![1, 0, 0, 0, 99];
        execute(&mut t1);
        assert_eq!(t1, [2, 0, 0, 0, 99]);

        let mut t1 = vec![2, 3, 0, 3, 99];
        execute(&mut t1);
        assert_eq!(t1, [2, 3, 0, 6, 99]);

        let mut t1 = vec![2, 4, 4, 5, 99, 0];
        execute(&mut t1);
        assert_eq!(t1, [2, 4, 4, 5, 99, 9801]);

        let mut t1 = vec![1, 1, 1, 4, 99, 5, 6, 0, 99];
        execute(&mut t1);
        assert_eq!(t1, [30, 1, 1, 4, 2, 5, 6, 0, 99]);
    }
}
