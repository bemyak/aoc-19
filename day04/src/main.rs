fn main() {
    let result = brute(278384, 824795);
    println!("{}", result);
}

fn brute(start: i32, end: i32) -> i32 {
    let mut count = 0;
    for i in start..end {
        if i.is_valid() {
            count += 1;
        }
    }
    count
}

trait Split: Sized {
    fn split(self) -> [i32; 6];
}

impl Split for i32 {
    fn split(self) -> [i32; 6] {
        let i1 = self / 1 % 10;
        let i2 = self / 10 % 10;
        let i3 = self / 100 % 10;
        let i4 = self / 1000 % 10;
        let i5 = self / 10000 % 10;
        let i6 = self / 100000 % 10;

        [i6, i5, i4, i3, i2, i1]
    }
}

trait Valid {
    fn is_valid(self) -> bool;
}

impl Valid for i32 {
    fn is_valid(self) -> bool {
        let digits = self.split();
        digits.is_valid()
    }
}

impl Valid for [i32; 6] {
    fn is_valid(self) -> bool {
        fn has_digits(me: &[i32; 6], ex1: usize, ex2: usize) -> bool {
            let t = me[ex1];
            for i in 0..me.len() {
                if i == ex1 || i == ex2 {
                    continue;
                }
                if me[i] == t {
                    return true;
                }
            }
            false
        }

        fn is_two_equal(me: &[i32; 6]) -> bool {
            for i in 1..me.len() {
                if me[i] == me[i - 1] && !has_digits(me, i - 1, i) {
                    return true;
                }
            }
            false
        }

        fn is_ascend(me: &[i32; 6]) -> bool {
            for i in 1..me.len() {
                if me[i] < me[i - 1] {
                    return false;
                }
            }
            true
        }

        is_two_equal(&self) && is_ascend(&self)
    }
}
