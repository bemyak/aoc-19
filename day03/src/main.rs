use std::{collections::HashMap, error::Error, fs, iter::repeat};

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
struct Point {
    x: i32,
    y: i32,
}

impl Point {
    fn shift(&mut self, direction: &Direction) {
        match direction {
            Direction::Up => self.y += 1,
            Direction::Down => self.y -= 1,
            Direction::Right => self.x += 1,
            Direction::Left => self.x -= 1,
        }
    }
}

#[derive(Debug, Clone)]
enum Direction {
    Up,
    Down,
    Right,
    Left,
}

impl Direction {
    fn from_str(s: &str) -> Vec<Self> {
        let (c, i) = s.split_at(1);
        let i = i.parse::<usize>().unwrap();
        let direction: Direction = c.into();
        repeat(direction).take(i).collect()
    }
}

impl From<&str> for Direction {
    fn from(s: &str) -> Self {
        match s {
            "U" => Direction::Up,
            "D" => Direction::Down,
            "R" => Direction::Right,
            "L" => Direction::Left,
            _ => panic!(),
        }
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    let input = fs::read_to_string("input")?;
    let len = process_input(&input);
    println!("Closest intersection = {}", len);
    Ok(())
}

fn process_input(input: &str) -> usize {
    let mut lines = input.lines().map(|line| {
        line.split(',')
            .map(|s| Direction::from_str(s))
            .flatten()
            .collect::<Vec<Direction>>()
    });

    let wire1 = lines.next().unwrap();
    let wire2 = lines.next().unwrap();

    let intersections = find_min_intersection_steps(&wire1, &wire2);

    let min = intersections.into_iter().min().unwrap();
    min
}

fn find_min_intersection_steps(wire1: &Vec<Direction>, wire2: &Vec<Direction>) -> Vec<usize> {
    let mut w1_points = HashMap::new();
    let mut p = Point { x: 0, y: 0 };
    for (i, dir) in wire1.iter().enumerate() {
        p.shift(dir);
        w1_points.insert(p.clone(), i);
    }
    let mut result = Vec::new();
    let mut p = Point { x: 0, y: 0 };
    for (i, dir) in wire2.iter().enumerate() {
        p.shift(dir);
        if let Some(steps) = w1_points.get(&p) {
            result.push(steps + i + 2)
        }
    }
    result
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_find_intersections() {
        let t1 = r"R8,U5,L5,D3
U7,R6,D4,L4";
        assert_eq!(process_input(t1), 30);
        let t1 = r"R75,D30,R83,U83,L12,D49,R71,U7,L72
U62,R66,U55,R34,D71,R55,D58,R83";
        assert_eq!(process_input(t1), 610);
        let t1 = r"R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51
U98,R91,D20,R16,D67,R40,U7,R15,U6,R7";
        assert_eq!(process_input(t1), 410);
    }
}
